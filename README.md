# Culvert calculator

The culvert calculator approximates the effect of flow through a culvert. The relationship between water levels, elevations, culvert characteristics and flow rate is based on the paper by [Boyd, 1987](https://gitlab.com/fantaz/culvert_calculator/tree/master/cc/papers/boyd1987.pdf). This implementation is greatly inspired by and closely follows the computational procedure found in [Gerris culvert module](http://gfs.sourceforge.net/wiki/index.php/GfsSourceCulvert).

## Example
```c++
#include <iostream>
#include "culvertcalc.h"
#include <boost/units/io.hpp>
int main()
{
    double HW, TW, B, D, L;
    double S0, N, KE;
    D = 1.0;   // Culvert height
    B = 1.0;   // Culvert width
    S0 = 0.01; // Slope
    L = 10.0;  // Culvert length
    N = 0.013; // Manning coefficient
    KE = 1.0;  // Entrance loss ceofficient
    HW = 19.6; // Head water
    TW = 1.4;  // Tailwater water

    // constructing the box culvert instance
    Culvert<Box> cbox(D,B,L, entranceType::wf_30_75, S0, N, KE);
    std::cout<<"Box culvert Q = "<<cbox(HW,TW)<<std::endl;

    // constructing the pipe culvert instance
    Culvert<Pipe> cpipe(D,L, entranceType::wf_30_75, S0, N, KE);
    std::cout<<"Pipe culvert Q = "<<cpipe(HW,TW)<<std::endl;

    // Alternatively, one can use boost::units system
    using namespace boost::units;
    using us_foot = quantity<us::foot_base_unit::unit_type>;
    using si_length = quantity<si::length>;
    using si_flow_rate = quantity<si::volume_flow_rate>;
    using us_flow_rate = quantity<us::volume_flow_rate>;

    us_foot b, d, l;          // setting input variables in feet
    si_length hw, tw;         // setting head and tailwater in meters
    b = us_foot(B*si::meter); // converting B to meters and then to feet
    d = us_foot(D*si::meter);
    l = us_foot(L*si::meter);
    tw = si_length(TW*si::meter);
    hw = si_length(HW*si::meter);

    // constructing culvert object - need to convert feet to meters
    Culvert<Box> units_culvert(si_length(d),si_length(b),si_length(l),
                               entranceType::wf_30_75, S0, N, KE);
    std::cout<<"input values: \n";
    std::cout<<"D = "<<d<<std::endl;
    std::cout<<"B = "<<b<<std::endl;
    std::cout<<"L = "<<l<<std::endl;
    std::cout<<"HW = "<<hw<<std::endl;
    std::cout<<"TW = "<<tw<<std::endl;

    // finally, result is in m3/s
    si_flow_rate si_discharge = units_culvert( hw, tw);
    us_flow_rate us_discharge (si_discharge); // convert to cubic feet per second

    std::cout<<"Q = "<<si_discharge<<std::endl;
    std::cout<<"Q = "<<us_discharge<<std::endl;
    return 0;
}
```


## Installation

### c++
The CulvertCalculator depends on [Boost library](http://www.boost.org).
Just include *culvertcalc.h* in your project, include boost directory and your're set.

### Python
import *culvertcalc* module
```python
import culvertcalc as cc
```
One can calculate culvert discharge with following functions:
```python
box_culvert(hw,tw, d=1, b=1, etype='wf_30_75', l=10, s0=0.01, n=0.03, ke=1.)
pipe_culvert(hw,tw, d=1, etype='wf_30_75', l=10, s0=0.01, n=0.03, ke=1.)
```
where:
* hw is headwater depth in meters
* tw is tailwater depth in meters
* d  is culvert height or diameter in meters, default=1
* b  is culvert width (*box_culvert* only) in meters, default=1
* etype is entrance type following Boyd, 1987. default='wf_30_75'
  * 'wf_30_75' is Wingwall Flare 30° to 75°
  * 'wf_90_15' is Wingwall Flare 90° and 15°
  * 'wf_0' is Wingwall Flare 0°
* l is culvert's length in meters, default=10
* s0 is culvert's slope, default=0.01
* n is Manning coefficient, default=0.03
* ke is entrance loss coefficient, default=1.

## Licence
The MIT License (MIT)

Copyright (c) 2015 Jerko Škifić

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.