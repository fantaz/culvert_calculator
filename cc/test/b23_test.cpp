// This file is part of CulvertCalculator.
//
// Copyright (c) 2015 Jerko Skific
// 
// CulvertCalculator is free software: you can redistribute it and/or modify
// it under the terms of the The MIT License
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#define BOOST_TEST_MODULE B23_TEST
#include <boost/test/included/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <boost/iterator/iterator_concepts.hpp>

#include "culvertcalc.h"

const double eps {0.000001};
BOOST_AUTO_TEST_CASE ( b23_test )
{

    double HW, TW, B, D, S0, L, N, KE;
    int type;
    B = 1.0;
    D = 1.0;
    type = 1;
    S0 = 0.01;
    L = 10.0;
    N = 0.013;
    KE = 1.0;

    printf ( "B D S0 L N KE\n" );
    printf ( "B=%g, D=%g, L=%g, S0=%g, N=%g, KE=%g\n", B, D, L, S0, N, KE);
    printf ( "type\n" );
    printf ( "%d\n", type );
    printf ( "HW TW Q_Box Q_Pipe\n" );

    Culvert<Box> c(D,B,L, entranceType::wf_30_75, S0, N, KE);
    for ( TW = 0.; TW <= 1.5*D; TW += 0.1 )
    {
        for ( HW = 0.1; HW <= 20.; HW += 0.5 )
        { printf ( "--->%g %g %g\n", HW, TW, c( HW, TW) ); }
        printf ( "\n" );
    }
    
}

#include <boost/units/io.hpp>
BOOST_AUTO_TEST_CASE ( unit_systems_test )
{
    using namespace boost::units;
    using us_foot = quantity<us::foot_base_unit::unit_type>;
    using si_length = quantity<si::length>;
    using si_flow_rate = quantity<si::volume_flow_rate>;
    using us_flow_rate = quantity<us::volume_flow_rate>;
    
    
    double hw{15}, tw{1.5}, b{1},d{1},l{10};
    us_foot HW, TW, B, D, L;
    
    double S0 {0.01};
    double N  {0.013};
    double KE {1.0};
    
    entranceType type {entranceType::wf_30_75};
    B = us_foot(b*si::meter);
    D = us_foot(d*si::meter);
    L = us_foot(l*si::meter);
    
    TW = us_foot(tw*si::meter);
    HW = us_foot(hw*si::meter);
    
    Culvert<Box> c(d,b,l, type, S0, N, KE);
    
    std::cout<<"input values as ordinary doubles (that is, using SI system): \n";
    std::cout<<"D = "<<d<<std::endl;
    std::cout<<"B = "<<b<<std::endl;
    std::cout<<"L = "<<l<<std::endl;
    std::cout<<"HW = "<<hw<<std::endl;
    std::cout<<"TW = "<<tw<<std::endl;
    double discharge = c( hw, tw);
    
    si_flow_rate si_q = (discharge*si::m3_s);
    us_flow_rate us_q (si_q);
    std::cout<<"Q = "<<discharge<<std::endl;
    std::cout<<"Q = "<<si_q<<std::endl;
    std::cout<<"Q = "<<us_q<<std::endl;
    
    Culvert<Box> c2(si_length(D),si_length(B),si_length(L), type, S0, N, KE);
    
    std::cout<<"input values using us::foot: \n";
    std::cout<<"D = "<<D<<std::endl;
    std::cout<<"B = "<<B<<std::endl;
    std::cout<<"L = "<<L<<std::endl;
    std::cout<<"HW = "<<HW<<std::endl;
    std::cout<<"TW = "<<TW<<std::endl;
    
    si_flow_rate si_q2 = c2( si_length(HW), si_length(TW));
    us_flow_rate us_q2 (si_q2);
    
    std::cout<<"Q = "<<si_q2<<std::endl;
    std::cout<<"Q = "<<us_q2<<std::endl;
    
    BOOST_REQUIRE_CLOSE( si_q2.value() , discharge, eps );
    BOOST_REQUIRE_CLOSE( us_q2.value() , us_q.value(), eps );
}
