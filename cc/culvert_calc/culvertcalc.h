// This file is part of CulvertCalculator.
//
// Copyright (c) 2015 Jerko Skific
// 
// CulvertCalculator is free software: you can redistribute it and/or modify
// it under the terms of the The MIT License
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.





#pragma once
#include <limits>
#include <cmath>
#include <boost/math/constants/constants.hpp>
#include <boost/math/tools/roots.hpp>

//! \brief gravity, obviously
const double g { 9.81 };

const double pi = boost::math::constants::pi<double>();
/**
 * Subclasses are used for root finding
 */
class OutletSolver
{
public:
    OutletSolver ( const double &_hw,const double &_tw,const double &_d,
                   const double &_b, const double &_l,const double &_s0, const double &_ke, const double& _n ) :
        HW ( _hw ),TW ( _tw ), D ( _d ), B ( _b ), L ( _l ), S0 ( _s0 ), Ke ( _ke ), N ( _n ) {}

    /**
     * \brief Checks if the outlet is submerged
     */
    bool isOutletSubmerged() const { return TW > D; }

    /**
     * \brief Calculates discharge for submerged outlet
     * Implemented in subclasses
     */
    virtual double calcSubmergedOutlet() const=0;

protected: // should be visible to subclasses
    /**
     * \brief calculates discharge based on wetted area and hydraulic radius using Bernoulli equation
     * Hence the name Bernoulli
     */
    double Bernoulli ( const double& area, const double& Rh ) const 
    {
        if ( area > 0. ) 
        {
            /* equations (11) and (12a) */
            double v2 = 2.*g*fabs ( HW + S0*L - TW ) / ( Ke + 1. + 2.*g*N*N*L/pow ( Rh, 1.333 ) );
            return area*sqrt ( v2 );
        }
        return 0.;
    }
    
    //! data members: headwater depth, tailwater depth, culvert diameter(or height),
    //! culvert width, culvert's length, slope, loss coefficient and Manning coefficient
    double HW,TW, D, B, L, S0, Ke, N;
};


class BoxOutletSolver : public OutletSolver
{
public:
    BoxOutletSolver ( const double &_hw,const double &_tw,const double &_d,
                      const double &_b, const double &_l,const double &_s0, const double &_ke, const double& _n ) :
        OutletSolver ( _hw, _tw, _d, _b, _l, _s0, _ke, _n ) {}

    /**
     * \brief Used to find root of the function (defined in return statement)
     */
    double operator () ( double x ) {
        double h0 = std::max ( ( criticalDepth ( x ) + D ) /2., TW );
        if ( h0 > D ) {
            h0 = D;
        }
        double area = B*h0;
        double Rh = B*h0/ ( B + 2.*h0 );
        return Bernoulli ( area,Rh )-x;
    }

    /**
     * \brief calculates critical depth for a given discharge
     */
    double criticalDepth ( double Q ) const {
        return 0.4672*std::pow ( Q/B, 0.667 ); // equation (1)
    }

    /**
     * \brief Calculates discharge for submerged outlet
     */
    double calcSubmergedOutlet() const override {
        return Bernoulli ( B*D,B*D/ ( B + 2.*D ) );
    }

};

class PipeOutletSolver : public OutletSolver
{
public:
    PipeOutletSolver ( const double &_hw,const double &_tw,const double &_d,
                       const double &_l,const double &_s0, const double &_ke, const double& _n ) :
        OutletSolver ( _hw, _tw, _d, std::numeric_limits<double>::quiet_NaN(), _l, _s0, _ke, _n ) {}

    double operator () ( double x ) {
        double h0 = std::max ( ( criticalDepth ( x ) + D ) /2., TW );
        if ( h0 > D ) {
            h0 = D;
        }
        double theta = acos ( 1. - 2.*h0/D );
        double B = D*sin ( theta );
        double area = D*D* ( theta - sin ( 2.*theta ) /2. ) /4.;
        double perimeter = B + theta*D;
        double Rh = area/perimeter;

        return Bernoulli ( area,Rh )-x;
    }

    /**
     * \brief calculates critical depth for a given discharge
     */
    double criticalDepth ( double Q ) const {
        double dc;
        dc = D*std::pow ( ( Q/std::sqrt ( g ) *std::pow ( D, 2.5 ) ) /1.26, 1./3.75 ); /* equation (4a) */

        if ( dc/D < 0.85 ) {
            dc = D*std::pow ( ( Q/std::sqrt ( g ) *std::pow ( D, 2.5 ) ) /0.95, 1./1.95 );    /* equation (4b) */
        }

        return dc;
    }
    /**
     * \brief Calculates discharge for submerged outlet
     */
    double calcSubmergedOutlet() const override {
        return Bernoulli ( pi*D*D/4.,D/4. );
    }


};

//! \brief Culvert entrance type
enum class entranceType
{
    wf_30_75,   // entrance type 1: Wingwall Flare 30 degrees to 75 degrees
    wf_90_15,   // entrance type 2: Wingwall Flare 90 degrees and 15 degrees
    wf_0        // entrance type 3: Wingwall Flare 0 degrees
};

/**
 * \brief Pipe culvert
 * It is defined by its diameter d,  length l , entrance type etype, slope s0,
 * Manning coefficient n and loss coefficient ke
 */

class Pipe
{
public:
    Pipe ( const double& _d, const double &_l, const entranceType& _etype,
           const double &_s0, const double &_n, const double &_ke ) :
        d ( _d ), l ( _l ), etype {_etype}, s0 ( _s0 ), n ( _n ), ke ( _ke ) {}

    /**
     * \brief Flow rate for circular pipe culvert, inlet control.
     * HW: headwater depth
     */
    double Q_inlet ( const double & HW ) const {
        double HW1 = HW;

        switch ( etype ) {
        case entranceType::wf_30_75: /* entrance type 1: Square edge with headwall */
            HW1 = HW;
            break;

        case entranceType::wf_90_15: /* entrance type 2: Groove end with headwall */
            HW1 = d*std::pow ( HW/d/0.92, 1./0.90 ); /* equation (10c) */
            break;

        case entranceType::wf_0: /* entrance type 3: Groove end projecting */
            HW1 = d*std::pow ( HW/d/0.91, 1./0.94 ); /* equation (10d) */
            break;
        }

        return Q_inlet_pipe ( HW1 );
    }

    /**
     * \brief Flow rate for circular pipe culvert, outlet control.
     * HW: headwater depth
     * TW: tailwater depth
     * If outlet is not submerged,
     */
    double Q_outlet ( const double &HW, const double &TW ) const {
        // contructing solver
        PipeOutletSolver solver ( HW,TW,d,l,s0,ke,n );
        /* if outlet submerged, Bernoulli equation solution based on HW and TW is valid */
        if ( solver.isOutletSubmerged() ) {
            return solver.calcSubmergedOutlet();
        }
        /* else outlet not submerged, Bernoulli should be solved with HW and
        flow dependent outlet waterlevel h0 */
        using boost::math::tools::toms748_solve;
        double from = 0;
        double to = 1e06;
        boost::uintmax_t max_iter=500;
        boost::math::tools::eps_tolerance<double> tol ( 30 );
        std::pair<double, double> result = toms748_solve ( solver,
                                           from, to, tol, max_iter );
        double root = ( result.first + result.second ) / 2;
        return root;
    }
    
private:
    /**
     * \brief If outlet is submerged, then it is easy to compute discharge
     */
    double Q_inlet_pipe ( double HW ) const {
        if ( HW/d < 1.2 )
            /* Inlet not submerged */
        {
            return 0.421*std::sqrt ( g ) *std::pow ( d, 0.87 ) *std::pow ( HW, 1.63 );    /* equation (10a) */
        } else
            /* Inlet submerged */
        {
            return 0.530*std::sqrt ( g ) *std::pow ( d, 1.87 ) *std::pow ( HW, 0.63 );    /* equation (10b) */
        }
    }
private:
    const double d;
    const double l;
    const entranceType etype;
    const double s0;
    const double n;
    const double ke;
};

class Box
{
public:
    Box ( const double& _d, const double& _b, const double &_l, const entranceType& _etype,
          const double &_s0, const double &_n, const double &_ke ) :
        d ( _d ), b ( _b ),  l ( _l ), etype {_etype}, s0 ( _s0 ), n ( _n ), ke ( _ke ) {}
public:
    double Q_outlet ( const double & HW, const double & TW ) const {
        BoxOutletSolver f ( HW,TW,d,b,l,s0,ke,n );
        /* if outlet submerged, Bernoulli equation solution based on HW and TW is valid */
        if ( f.isOutletSubmerged() ) {
            return f.calcSubmergedOutlet();
        }

        /* else outlet not submerged, Bernoulli should be solved with HW and
        flow dependent outlet waterlevel h0 */
        using boost::math::tools::bisect;
        using boost::math::tools::toms748_solve;
        double from = 0;
        double to = 1e06;
        boost::uintmax_t max_iter=500;
        boost::math::tools::eps_tolerance<double> tol ( 30 );
        std::pair<double, double> result = toms748_solve ( f,
                                           from, to, tol, max_iter );
        double root = ( result.first + result.second ) / 2;
        return root;
    }
    /**
     * Flow rate for box culvert, inlet control.
     * HW: headwater depth
     */
    double Q_inlet ( const double & HW ) const {
        double HW1 = HW;

        switch ( etype ) {
        case entranceType::wf_30_75: /* entrance type 1: Wingwall Flare 30 degrees to 75 degrees */
            HW1 = HW;
            break;

        case entranceType::wf_90_15: /* entrance type 2: Wingwall Flare 90 degrees and 15 degrees */
            HW1 = d*std::pow ( HW/d/1.09, 1./0.99 ); /* equation (9c) */
            break;

        case entranceType::wf_0: /* entrance type 3: Wingwall Flare 0 degrees */
            HW1 = d*std::pow ( HW/d/1.07, 1./1.08 ); /* equation (9d) */
            break;
        }

        return Q_inlet_box ( HW1 );
    }
private:
    double Q_inlet_box ( double HW ) const {
        if ( HW/d < 1.35 )
            /* Inlet not submerged */
        {
            return 0.544*std::sqrt ( g ) *b*std::pow ( HW, 1.50 );    /* equation (9a) */
        } else
            /* Inlet submerged */
        {
            return 0.702*std::sqrt ( g ) *b*std::pow ( d, 0.89 ) *std::pow ( HW, 0.61 );    /* equation (9b) */
        }
    }
private:
    const double d;
    const double b;
    const double l;
    const entranceType etype;
    const double s0;
    const double n;
    const double ke;
};

// declaring volume flow rate for  convenience

#include <boost/units/derived_dimension.hpp>
#include <boost/units/physical_dimensions/length.hpp>
#include <boost/units/physical_dimensions/time.hpp>

namespace boost {

    namespace units {

        /// derived dimension for volume : l^3 T^-1 
        typedef derived_dimension<length_base_dimension,3,time_base_dimension,-1>::type volume_flow_rate_dimension;

    } // namespace units

}


#include <boost/units/systems/si.hpp>
#include <boost/units/systems/si/base.hpp>
namespace boost {

    namespace units { 

        namespace si {

            typedef unit<volume_flow_rate_dimension,si::system>      volume_flow_rate;

            BOOST_UNITS_STATIC_CONSTANT(cubic_meter_per_second,volume_flow_rate);    
            BOOST_UNITS_STATIC_CONSTANT(cubic_meters_per_second,volume_flow_rate);   
            BOOST_UNITS_STATIC_CONSTANT(cubic_metre_per_second,volume_flow_rate);    
            BOOST_UNITS_STATIC_CONSTANT(cubic_metres_per_second,volume_flow_rate);
            BOOST_UNITS_STATIC_CONSTANT(m3_s,volume_flow_rate);

        } // namespace si

    } // namespace units

} // namespace boost

#include <boost/units/base_units/us/foot.hpp>

namespace boost {
    namespace units {
        namespace us {
            typedef make_system<foot_base_unit, si::second_base_unit>::type system;
            typedef unit<volume_flow_rate_dimension, system> volume_flow_rate;

            BOOST_UNITS_STATIC_CONSTANT(cft_s,volume_flow_rate);
            BOOST_UNITS_STATIC_CONSTANT(ft3_s,volume_flow_rate);

        }
    }
}

template<class Element>
class Culvert
{
    using si_length = boost::units::quantity<boost::units::si::length>;
    using si_flow_rate = boost::units::quantity<boost::units::si::volume_flow_rate>;
    
public:
    // pipe ctors
    Culvert ( double& _d, const double &_l, const entranceType& _etype,
              const double &_s0, const double &_n, const double &_ke ) :
        el ( _d,_l, _etype, _s0, _n, _ke )
    {}
    Culvert ( const si_length& _d, 
              const si_length& _l, 
              const entranceType& _etype,const double &_s0, const double &_n, const double &_ke ) :
        Culvert ( _d/boost::units::si::meter, 
                  _l/boost::units::si::meter, _etype, _s0, _n, _ke )
    {}
    // box ctors
    Culvert ( const double& _d, const double& _b, const double &_l, const entranceType& _etype,
              const double &_s0, const double &_n, const double &_ke ) :
        el ( _d, _b, _l, _etype, _s0, _n, _ke )
    {}
    Culvert ( const si_length& _d, 
              const si_length& _b, 
              const si_length& _l, 
              const entranceType& _etype,const double &_s0, const double &_n, const double &_ke ) :
        Culvert ( _d/boost::units::si::meter, 
                  _b/boost::units::si::meter, 
                  _l/boost::units::si::meter, _etype, _s0, _n, _ke )
    {}
    // operators ()
    double operator () ( const double &hw, const double &tw ) const {
        return std::min ( el.Q_outlet ( hw,tw ), el.Q_inlet ( hw ) );
    }
    
    si_flow_rate operator () (const si_length& hw, const si_length &tw ) const
    {
        return si_flow_rate ( 
                            std::min ( el.Q_outlet ( hw/boost::units::si::meter,
                                                     tw/boost::units::si::meter ), 
                                        el.Q_inlet ( hw/boost::units::si::meter ) 
                                      )*boost::units::si::m3_s
                             );
    }
private:
    Element el;
};






