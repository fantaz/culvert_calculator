// This file is part of CulvertCalculator.
//
// Copyright (c) 2015 Jerko Skific
// 
// CulvertCalculator is free software: you can redistribute it and/or modify
// it under the terms of the The MIT License
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "culvertcalc.h"
#include <iostream>
#include <boost/python.hpp>


#include <string>
using namespace boost::python;

entranceType string_to_entranceType(const std::string& etype)
{
    if      (etype=="wf_30_75") return entranceType::wf_30_75;
    else if (etype=="wf_90_15") return entranceType::wf_90_15;
    else if (etype=="wf_0")     return entranceType::wf_0;
    else
    {
        std::cerr<<"invalid entranceType: "<< etype<<".\nDefaulting to entranceType::wf_30_75\n";
        return entranceType::wf_30_75;
    }
}
double box_culvert(double hw, double tw, double d, double b, std::string etype, double l, double s0, double n, double ke)
{
    
    Culvert<Box> c(d,b,l,string_to_entranceType(etype),s0,n,ke);
    return c(hw,tw);
}

double pipe_culvert(double HW, double TW, double D, std::string etype, double L, double S0, double N, double Ke)
{
    Culvert<Pipe> c(D,L,string_to_entranceType(etype),S0,N,Ke);
    return c(HW,TW);
}

void entrance_types()
{
    std::cerr<<"wf_30_75, wf_90_15, wf_0\n";
}

boost::python::tuple  fun()
{
    return boost::python::make_tuple(1.4, 14., "neki string");
}

BOOST_PYTHON_MODULE(culvertcalc)
{

    docstring_options local_docstring_options(true, true, false);
    def("box_culvert", box_culvert,
        (arg("hw"),
        arg("tw"),
        arg("d")=1,
        arg("b")=1,
        arg("etype")="wf_30_75",
        arg("l")=10,
        arg("s0")=0.01,
        arg("n")=0.013,
        arg("ke")=1.), "culverts some noise"
    );
    
    def("pipe_culvert", pipe_culvert,
        (arg("hw"),
        arg("tw"),
        arg("d")=1,
        arg("etype")="wf_30_75",
        arg("l")=10,
        arg("s0")=0.01,
        arg("n")=0.013,
        arg("ke")=1.)
    );
    
    def("entrance_types", entrance_types);
    def("fun", fun);
    
}
